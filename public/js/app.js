(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function (options) {
    var _clockElement = null
    var _clockHoursElement = null
    var _clockSeparatorElement = null
    var _clockMinutesElement = null
    var _clockTimer = null
    var _clockStarted = false
    var _currentTime = null
    var _currentHumanTime = {
        hours: '00',
        minutes: '00'
    }

    var _initClockElement = function() {
        // get the clock element
        _clockElement = document.querySelector(options.element)
        
        // error during process
        if(!_clockElement) return false

        // create Hours element
        _clockHoursElement = document.createElement('span')
        _clockHoursElement.classList.add('clock-hours')
        _clockHoursElement.textContent = '00'
        

        // create Separator element
        _clockSeparatorElement = document.createElement('span')
        _clockSeparatorElement.classList.add('clock-separator')
        _clockSeparatorElement.textContent = ' : '

        // create Minutes element
        _clockMinutesElement = document.createElement('span')
        _clockMinutesElement.classList.add('clock-minutes')
        _clockMinutesElement.textContent = '00'

        // append clock element with each element needed
        _clockElement.appendChild(_clockHoursElement)
        _clockElement.appendChild(_clockSeparatorElement)
        _clockElement.appendChild(_clockMinutesElement)

        // init clock success
        return true
    }

    _normalize = function(n) {
        return ('00' + n).slice(-2)
    }
    
    _getHumanTime = function(d) {
        var date = new Date(d)
        return {
            hours : _normalize(date.getHours()),
            minutes : _normalize(date.getMinutes())
        }
    }

    _updateTime = function () {
        _currentTime = Date.now()
        _currentHumanTime = _getHumanTime(_currentTime)

        console.log('[CLOCK] UPDATE TIME', _currentHumanTime)
        
        _clockHoursElement.textContent = _currentHumanTime.hours
        _clockMinutesElement.textContent = _currentHumanTime.minutes

        // automatize
        clearTimeout(_clockTimer)
        _clockTimer = setTimeout(_updateTime, 60000)
    }
    
    this.getHumanTime = _getHumanTime

    this.getTime = function() {
        return {
            timestamp: _currentTime,
            human: _currentHumanTime
        }
    }


    this.stop = function() {
        clearTimeout(_clockTimer)
    }
    
    this.start = function () {
        if(_initClockElement()) {
            console.log('[CLOCK] INIT', _clockElement)
            clearTimeout(_clockTimer)
            _updateTime()
        }
        else {
            console.error('[TIMELINE] An error has occured XD')
        }
    }
}
},{}],2:[function(require,module,exports){
module.exports = function(options) {
    _slideshowElement = null
    _slideshowElements = []
    _currentSlideshowIndex = 0

    _show = function(i) {
        _slideshowElements[i].classList.add('active')
    }

    _hide = function(i) {
        _slideshowElements[i].classList.remove('active')
    }

    _initImgList = function() {
        _slideshowElements = _slideshowElement.querySelectorAll('img')
        return !!_slideshowElements.length
    }

    _updateSlideShow = function() {
        console.log('[SLIDESHOW] update')
        // hide current img
        _hide(_currentSlideshowIndex)

        // update index
        _currentSlideshowIndex += 1
        if(_currentSlideshowIndex >= _slideshowElements.length) _currentSlideshowIndex = 0

        // show new current img
        _show(_currentSlideshowIndex)
        
        // repeat
        setTimeout(_updateSlideShow, 10000)
    }

    this.init = function() {
        _slideshowElement = document.querySelector(options.element)
        if(_slideshowElement && _initImgList()) {
            console.log('[SLIDESHOW] init')
            // start slideShow
            _updateSlideShow()
        }
        else {
            console.error('[SLIDESHOW] An error has occured XD')
        }
    }
}
},{}],3:[function(require,module,exports){
module.exports = function(options) {
    _element = null
    _elements = []
    _data = [
        [
            { name: 'Babar', time: 1498239000000 },
            { name: 'M.A. SUET', time: 1498242600000 },
            { name: 'Le Pied De La Pompe', time: 1498247400000 },
            { name: 'Feu + Bagad', time: 1498251000000 },
            { name: 'Maracujah', time: 1498253700000 },
            { name: 'Babar', time: 1498258800000 }
        ],
        [
            { name: 'Bukatribe', time: 1498321800000 },
            { name: 'Archimède', time: 1498325700000 },
            { name: 'Jive Me', time: 1498331400000 },
            { name: 'Biga*Ranx', time: 1498338000000 },
            { name: 'Monty Picon', time: 1498344300000 }
        ]
    ]
    
    _day = 0

    _initDom = function(data) {
        // empty elements array
        _elements = []
        // empty dom
        while (_element.firstChild) {
            _element.removeChild(_element.firstChild);
        }

        data.forEach(function (group) {
            var groupHumanTime = options.clock.getHumanTime(group.time)
            var groupEl = document.createElement('div')
            
            groupEl.classList.add('timeline-item')
            groupEl.setAttribute('data-timestamp', group.time)
            groupEl.setAttribute('data-horaire', groupHumanTime.hours + ":" + groupHumanTime.minutes)
            
            var groupNameEl = document.createElement('h3')
            groupNameEl.textContent = group.name

            groupEl.appendChild(groupNameEl)
            _element.appendChild(groupEl)

            _elements.push(groupEl)
        })
    }

    _updateDom = function(data) {
        if(!_elements.length || _elements.length !== data.length) _initDom(data)
        
        var now = Date.now()
        for(var i = 0; i < data.length; i++) {
            // clear classes
            _elements[i].classList.remove('active')
            _elements[i].classList.remove('done')
            if(now >= data[i].time) {
                if(data[i+1] && now < data[i+1].time) {
                    _elements[i].classList.add('active')
                } else {
                    _elements[i].classList.add('done')
                }
            }
        }
    }

    _updateTimeline = function() {
        // set day
        var oldDay = _day
        if(options.clock.getTime().timestamp > 1498298400000) _day = 1
        console.log('[TIMELINE] day', _day)

        // if day has changed reset dom
        if(oldDay !== _day) _initDom(_data[_day])

        // update Dom
        _updateDom(_data[_day])
        
        setTimeout(_updateTimeline, 60000)
    }

    this.init = function() {
        _element = document.querySelector(options.element)

        if(_element) {
            console.log('[TIMELINE] INIT', options)
            _initDom(_data[_day])
            _updateTimeline()
        }
        else {
            console.error('[TIMELINE] An error has occured XD')
        }
    }
}
},{}],4:[function(require,module,exports){
var Clock = require('./_clock')
var Timeline = require('./_timeline')
var Slideshow = require('./_slideshow')

var clock = new Clock({ element: '.clock' })
clock.start()

var timeline = new Timeline({ element: '.timeline', clock: clock })
timeline.init()

var slideshow = new Slideshow({ element: '.gallery' })
slideshow.init()
},{"./_clock":1,"./_slideshow":2,"./_timeline":3}]},{},[4]);
