var Clock = require('./_clock')
var Timeline = require('./_timeline')
var Slideshow = require('./_slideshow')

var clock = new Clock({ element: '.clock' })
clock.start()

var timeline = new Timeline({ element: '.timeline', clock: clock })
timeline.init()

var slideshow = new Slideshow({ element: '.gallery' })
slideshow.init()