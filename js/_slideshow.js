module.exports = function(options) {
    _slideshowElement = null
    _slideshowElements = []
    _currentSlideshowIndex = 0

    _show = function(i) {
        _slideshowElements[i].classList.add('active')
    }

    _hide = function(i) {
        _slideshowElements[i].classList.remove('active')
    }

    _initImgList = function() {
        _slideshowElements = _slideshowElement.querySelectorAll('img')
        return !!_slideshowElements.length
    }

    _updateSlideShow = function() {
        console.log('[SLIDESHOW] update')
        // hide current img
        _hide(_currentSlideshowIndex)

        // update index
        _currentSlideshowIndex += 1
        if(_currentSlideshowIndex >= _slideshowElements.length) _currentSlideshowIndex = 0

        // show new current img
        _show(_currentSlideshowIndex)
        
        // repeat
        setTimeout(_updateSlideShow, 10000)
    }

    this.init = function() {
        _slideshowElement = document.querySelector(options.element)
        if(_slideshowElement && _initImgList()) {
            console.log('[SLIDESHOW] init')
            // start slideShow
            _updateSlideShow()
        }
        else {
            console.error('[SLIDESHOW] An error has occured XD')
        }
    }
}