module.exports = function (options) {
    var _clockElement = null
    var _clockHoursElement = null
    var _clockSeparatorElement = null
    var _clockMinutesElement = null
    var _clockTimer = null
    var _clockStarted = false
    var _currentTime = null
    var _currentHumanTime = {
        hours: '00',
        minutes: '00'
    }

    var _initClockElement = function() {
        // get the clock element
        _clockElement = document.querySelector(options.element)
        
        // error during process
        if(!_clockElement) return false

        // create Hours element
        _clockHoursElement = document.createElement('span')
        _clockHoursElement.classList.add('clock-hours')
        _clockHoursElement.textContent = '00'
        

        // create Separator element
        _clockSeparatorElement = document.createElement('span')
        _clockSeparatorElement.classList.add('clock-separator')
        _clockSeparatorElement.textContent = ' : '

        // create Minutes element
        _clockMinutesElement = document.createElement('span')
        _clockMinutesElement.classList.add('clock-minutes')
        _clockMinutesElement.textContent = '00'

        // append clock element with each element needed
        _clockElement.appendChild(_clockHoursElement)
        _clockElement.appendChild(_clockSeparatorElement)
        _clockElement.appendChild(_clockMinutesElement)

        // init clock success
        return true
    }

    _normalize = function(n) {
        return ('00' + n).slice(-2)
    }
    
    _getHumanTime = function(d) {
        var date = new Date(d)
        return {
            hours : _normalize(date.getHours()),
            minutes : _normalize(date.getMinutes())
        }
    }

    _updateTime = function () {
        _currentTime = Date.now()
        _currentHumanTime = _getHumanTime(_currentTime)

        console.log('[CLOCK] UPDATE TIME', _currentHumanTime)
        
        _clockHoursElement.textContent = _currentHumanTime.hours
        _clockMinutesElement.textContent = _currentHumanTime.minutes

        // automatize
        clearTimeout(_clockTimer)
        _clockTimer = setTimeout(_updateTime, 60000)
    }
    
    this.getHumanTime = _getHumanTime

    this.getTime = function() {
        return {
            timestamp: _currentTime,
            human: _currentHumanTime
        }
    }


    this.stop = function() {
        clearTimeout(_clockTimer)
    }
    
    this.start = function () {
        if(_initClockElement()) {
            console.log('[CLOCK] INIT', _clockElement)
            clearTimeout(_clockTimer)
            _updateTime()
        }
        else {
            console.error('[TIMELINE] An error has occured XD')
        }
    }
}