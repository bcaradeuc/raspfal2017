module.exports = function(options) {
    _element = null
    _elements = []
    _data = [
        [
            { name: 'Babar', time: 1498239000000 },
            { name: 'M.A. SUET', time: 1498242600000 },
            { name: 'Le Pied De La Pompe', time: 1498247400000 },
            { name: 'Feu + Bagad', time: 1498251000000 },
            { name: 'Maracujah', time: 1498253700000 },
            { name: 'Babar', time: 1498258800000 }
        ],
        [
            { name: 'Bukatribe', time: 1498321800000 },
            { name: 'Archimède', time: 1498325700000 },
            { name: 'Jive Me', time: 1498331400000 },
            { name: 'Biga*Ranx', time: 1498338000000 },
            { name: 'Monty Picon', time: 1498344300000 }
        ]
    ]
    
    _day = 0

    _initDom = function(data) {
        // empty elements array
        _elements = []
        // empty dom
        while (_element.firstChild) {
            _element.removeChild(_element.firstChild);
        }

        data.forEach(function (group) {
            var groupHumanTime = options.clock.getHumanTime(group.time)
            var groupEl = document.createElement('div')
            
            groupEl.classList.add('timeline-item')
            groupEl.setAttribute('data-timestamp', group.time)
            groupEl.setAttribute('data-horaire', groupHumanTime.hours + ":" + groupHumanTime.minutes)
            
            var groupNameEl = document.createElement('h3')
            groupNameEl.textContent = group.name

            groupEl.appendChild(groupNameEl)
            _element.appendChild(groupEl)

            _elements.push(groupEl)
        })
    }

    _updateDom = function(data) {
        if(!_elements.length || _elements.length !== data.length) _initDom(data)
        
        var now = Date.now()
        for(var i = 0; i < data.length; i++) {
            // clear classes
            _elements[i].classList.remove('active')
            _elements[i].classList.remove('done')
            if(now >= data[i].time) {
                if(data[i+1] && now < data[i+1].time) {
                    _elements[i].classList.add('active')
                } else {
                    _elements[i].classList.add('done')
                }
            }
        }
    }

    _updateTimeline = function() {
        // set day
        var oldDay = _day
        if(options.clock.getTime().timestamp > 1498298400000) _day = 1
        console.log('[TIMELINE] day', _day)

        // if day has changed reset dom
        if(oldDay !== _day) _initDom(_data[_day])

        // update Dom
        _updateDom(_data[_day])
        
        setTimeout(_updateTimeline, 60000)
    }

    this.init = function() {
        _element = document.querySelector(options.element)

        if(_element) {
            console.log('[TIMELINE] INIT', options)
            _initDom(_data[_day])
            _updateTimeline()
        }
        else {
            console.error('[TIMELINE] An error has occured XD')
        }
    }
}